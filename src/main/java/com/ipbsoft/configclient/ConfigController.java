package com.ipbsoft.configclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigController {

  @Value("${rate}")
  String rate;

  @Value("${start}")
  String start;

  @Value("${count}")
  String count;

  @Value("${connstring}")
  String connstring;

  @GetMapping(value = "/rate")
  public String getRate() {
    return rate;
  }

  @GetMapping(value = "/connstring")
  public String getConnstring() {
    return connstring;
  }

}
